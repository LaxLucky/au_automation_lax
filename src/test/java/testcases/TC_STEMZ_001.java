package testcases;

import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import stemz.health.resource.base.*;
import stemz.health.utilis.InvokeBrowser;
import stemz.health.pages.LoginPage;
import stemz.health.pages.TravelInfo;
@Listeners(stemz.health.utilis.TestListener.class)
public class TC_STEMZ_001 extends ProjectSpecificMethods{
	
	
	private boolean skip = false;
	@Test(dataProvider="StemzExecution")		
	public void registartion(String Iteration, String AutomationId,String RunMode, String Browser, String Scenario,String Platform, String PNR,String Airlines, 
			String identityType, String identityNumber, String Departure, String Arrival, String DepartDate, String ArriveDate) throws Exception {
		test.set(extent.createTest(Scenario));
		if (!RunMode.equalsIgnoreCase("Y")) {

			skip = true;
			throw new SkipException("Runmode is set No for the testcase " + Scenario);
			}
		System.out.println("****" + Iteration + "." + Scenario + " started****");
		switch (Iteration) {
		case "1":
			InvokeBrowser.get().invokeApplication(Platform);
			TravelInfo.get().fillTravelInfoPage(PNR, Airlines, identityType, identityNumber, Departure, Arrival, DepartDate, ArriveDate);
			Thread.sleep(5000);
			
		break;
		
		default:
			System.out.println("No such Scenarios exists please check the scenario name.");

			}
		System.out.println("****" + Iteration + "." + Scenario + " Finished****");
		}
//		LoginPage page= new LoginPage();
//		page.guestLogin();
//		TravelInfo ti=new TravelInfo();
//		ti.flightType();
//		ti.pnr(PNR);
		
}
