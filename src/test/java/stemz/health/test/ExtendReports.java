package stemz.health.test;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import stemz.health.resource.base.*;

public class ExtendReports extends ProjectSpecificMethods{

	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	ExtentTest test;

	@BeforeSuite
	public void reportSetup() {

		htmlReporter = new ExtentHtmlReporter("stemzReport.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

	}

	@Test
	public void testReport() throws IOException {
		test = extent.createTest("MyFirstTest", "Sample description");
		test.log(Status.INFO, "This step shows usage of log(status, details)");
		test.info("This step shows usage of info(details)");
		test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath("screenshot.png").build());
		test.addScreenCaptureFromPath("screenshot.png");

	}

	@AfterSuite
	public void reportDown() {
		 extent.flush();
	}
}
