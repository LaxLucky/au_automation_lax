package stemz.health.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import stemz.health.resource.base.*;

public class CallCentrePortal extends ProjectSpecificMethods{
	private JavascriptExecutor jse = null;
	private WebDriver driver = null;
	
	@FindBy(xpath = "//a//span[contains(text(),'Outbound Calls')]")
	private WebElement CCmodOutboundcall;
	
	@FindBy(xpath = "//a//span[contains(text(),'Inbound Calls')]")
	private WebElement CCmodInboundcall;
	
	@FindBy(xpath = "//a//span[contains(text(),'New Booking')]")
	private WebElement CCmodNewBooking;
	
	@FindBy(xpath = "//a//span[contains(text(),'New Complaint')]")
	private WebElement CCmodComplaint;
	
	@FindBy(xpath = "//a//span[contains(text(),'Tele Consult')]")
	private WebElement CCmodTeleconsult;
	
	@FindBy(xpath = "//div//mat-select[contains(@role,'listbox')]")
	private WebElement CCmodUserStatus;
	
	@FindBy(xpath = "//div//input[contains(@value,'today')]")
	private WebElement CCmodSelectToday;
	
	@FindBy(xpath = "//div//input[contains(@value,'oneweek')]")
	private WebElement CCmodSelectOneWeek;
	
	@FindBy(xpath = "//div//input[contains(@value,'twoweek')]")
	private WebElement CCmodSelectTwoWeek;
	
	@FindBy(xpath = "//div//input[contains(@value,'onemonth')]")
	private WebElement CCmodSelectOneMonth;
	
	@FindBy(xpath = "//div//span[@class='mat-button-wrapper' and contains(text(),'Search')]")
	private WebElement CCmodSearch;
	
	@FindBy(xpath = "//div//span[@class='mat-button-wrapper' and contains(text(),'Clear')]")
	private WebElement CCmodClear;
	
	@FindBy(xpath = "(//mat-select[contains(@role,'listbox')])[1]")
	private WebElement CCmodCallStatus;
	
	@FindBy(xpath = "(//mat-select[contains(@role,'listbox')])[1]")
	private WebElement CCmodResolutionStatus;
	
	@FindBy(xpath = "(//div[contains(@class,'mat-form-field-infix')])[3]")
	private WebElement CCmodHealthpassNo;	
	
	@FindBy(xpath = "//a[@href='#/changepassword']")
	private WebElement CCmodChangePassword;
	
	@FindBy(xpath = "//div//input[@placeholder='Old Password']")
	private WebElement CCmodOldPassword;
	
	@FindBy(xpath = "//div//input[@placeholder='New Password']")
	private WebElement CCmodNewPassword;
	
	@FindBy(xpath = "//div//input[@placeholder='Confirm Password']")
	private WebElement CCmodConfirmPassword;
	
	@FindBy(xpath = "//div//button[contains(text(),' Save ')]")
	private WebElement CCmodSave;
	
	@FindBy(xpath = "//div[contains(@class,'row logout')]")
	private WebElement CCmodLogOut;
	
	@FindBy(xpath = "//div//input[contains(@placeholder,'Health Passport / PNR Number')]")
	private WebElement CCmodPassnoSearch;
	
	@FindBy(xpath = "//div//span//mat-label[text()='Name']")
	private WebElement CCmodComplaintName;
	
	@FindBy(xpath = "//div//span//mat-label[text()='Mobile']")
	private WebElement CCmodComplaintMobileno;
	
	@FindBy(xpath = "//div//span//mat-label[text()='Email']")
	private WebElement CCmodComplaintEmail;
	
	@FindBy(xpath = "//div//span//mat-label[text()='Complaint Type']")
	private WebElement CCmodComplaintType;
	
	@FindBy(xpath = "//div//textarea[@placeholder='Call note']")
	private WebElement CCmodComplaintCallNote;
	
	@FindBy(xpath = "//div//button[@class='e-flat'and contains(text(),'Clear')]")
	private WebElement CCmodComplaintClear;
	
	@FindBy(xpath = "//div//button[@class='e-flat'and contains(text(),'Save')]")
	private WebElement CCmodComplaintSave;
	
	public CallCentrePortal(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
		jse = (JavascriptExecutor) driver;	
	}
}
