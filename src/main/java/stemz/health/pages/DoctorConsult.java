package stemz.health.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import stemz.health.resource.base.*;

public class DoctorConsult extends ProjectSpecificMethods  {
	private JavascriptExecutor jse = null;
	private WebDriver driver = null;
	
	@FindBy(xpath = "//div//mat-select[@formcontrolname='physicianLang']")
	private WebElement PhysicianLang;
	
	@FindBy(xpath = "//div//input[@formcontrolname='city']")
	private WebElement City;
	
	public DoctorConsult(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
		jse = (JavascriptExecutor) driver;	
	}
}
