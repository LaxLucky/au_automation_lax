package stemz.health.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import stemz.health.resource.base.*;

public class LabMasterPortal extends ProjectSpecificMethods {
	private JavascriptExecutor jse = null;
	private WebDriver driver = null;
	
	public LabMasterPortal(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
		jse = (JavascriptExecutor) driver;
	}
	
	@FindBy(xpath = "//a//i[contains(@class,'fa fa-flask')]")
	private WebElement labWorkList;
	
	@FindBy(xpath = "//a//i[contains(@class,'fa fa-line-chart')]")
	private WebElement labProgressEntry;
	
	@FindBy(xpath = "//a//i[contains(@class,'fa fa-clipboard')]")
	private WebElement labResultUpload;
	
	@FindBy(xpath = "//a//i[contains(@class,'fa fa-user-circle-o')]")
	private WebElement labUser;
	
	@FindBy(xpath = "//a//i[contains(@class,'fa fa-book')]")
	private WebElement labDiary;
	
	@FindBy(xpath = "//a//i[contains(@class,'fa fa-briefcase')]")
	private WebElement labServiceMaster;
	
	@FindBy(xpath = "//a[@href='#/changepassword']")
	private WebElement labChangePassword;
	
	@FindBy(xpath = "//div[contains(@class,'row logout')]")
	private WebElement labLogOut;
	
	@FindBy(xpath = "//div//input[@placeholder='Old Password']")
	private WebElement labOldPassword;
	
	@FindBy(xpath = "//div//input[@placeholder='New Password']")
	private WebElement labNewPassword;
	
	@FindBy(xpath = "//div//input[@placeholder='Confirm Password']")
	private WebElement labConfirmPassword;
	
	@FindBy(xpath = "//div//button[contains(text(),' Save ')]")
	private WebElement labSave;
	
	@FindBy(xpath = "(//div[contains(@class,'mat-form-field-infix')])[1]")
	private WebElement labUserName;
	
	@FindBy(xpath = "(//div[contains(@class,'mat-form-field-infix')])[2]")
	private WebElement labHealthPassportNo;
	
	@FindBy(xpath = "//div//span[@class='mat-button-wrapper' and contains(text(),'Search')]")
	private WebElement labSearch;
	
	@FindBy(xpath = "//div//span[@class='mat-button-wrapper' and contains(text(),'Clear')]")
	private WebElement labClear;
	
	@FindBy(xpath = "//div//p[contains(text(),'Pending')]")
	private WebElement labPending;
	
	@FindBy(xpath = "//div//p[contains(text(),'In Progress')]")
	private WebElement labProgres;
	
	@FindBy(xpath = "//div//p[contains(text(),'Completed')]")
	private WebElement labCompleted;
	
	@FindBy(xpath = "//div//mat-label[contains(text(),'Filter')]")
	private WebElement labFilter;
	
	@FindBy(xpath = "//div//mat-select[contains(@role,'listbox')]")
	private WebElement labUserStatus;
	
	@FindBy(xpath = "//div//input[contains(@value,'today')]")
	private WebElement labSelectToday;
	
	@FindBy(xpath = "//div//input[contains(@value,'oneweek')]")
	private WebElement labSelectOneWeek;
	
	@FindBy(xpath = "//div//input[contains(@value,'twoweek')]")
	private WebElement labSelectTwoWeek;
	
	@FindBy(xpath = "//div//input[contains(@value,'onemonth')]")
	private WebElement labSelectOneMonth;
	
	@FindBy(xpath = "//div[contains(@col-id,'action')]//i[@class='fa fa-th']")
	private WebElement labUserAction;
	
	@FindBy(xpath = "//div//button[contains(text(),'Assigned ')]")
	private WebElement labClickAssigned;
	
	@FindBy(xpath = "//div//button[contains(text(),'Ok')]")
	private WebElement labClickOk;
	
	@FindBy(xpath = "//div//button[contains(text(),'Cancel')]")
	private WebElement labClickCancel;
	
	@FindBy(xpath = "//button//div[contains(text(),'Collected')]")
	private WebElement labClickCollected;
	
	@FindBy(xpath = "//button[contains(@class,'ng-star-inserted')and contains(text(),' Not Collected ')]")
	private WebElement labClickNotCollected;
	
	@FindBy(xpath = "//div//input[contains(@placeholder,'Health Passport / PNR Number')]")
	private WebElement labPassNosearch;
	
	@FindBy(xpath = "//div[@class='upload ng-star-inserted']//input[@id='file']")
	private WebElement labFileChoosen;
	
	@FindBy(xpath = "(//i[contains(@class,'fa fa-cloud-upload')])[2]")
	private WebElement labFileUpload;
	
	@FindBy(xpath = "//div//span[contains(text(),'Negative')]")
	private WebElement labTestNegative;
	
	@FindBy(xpath = "//div//span[contains(text(),'Positive')]")
	private WebElement labTestPositive;
	
	@FindBy(xpath = "//div//span[contains(text(),'Inconclusive')]")
	private WebElement labTestInconclusive;
	
	@FindBy(xpath = "//i[@class='fa fa-floppy-o ng-star-inserted']")
	private WebElement labFileSave;
	
	@FindBy(xpath = "(//div[contains(@class,'mat-form-field-infix')]//span[text()='Site'])")
	private WebElement labUserSite;
	
	@FindBy(xpath = "(//div[contains(@class,'mat-select-value')])")
	private WebElement labUserType;
	
	@FindBy(xpath = "//i[@class='fa fa-search']")
	private WebElement labUserSearch;
	
	@FindBy(xpath = "//i[@class='fa fa-user-plus']")
	private WebElement labAddUser;
	
	@FindBy(xpath = "(//div//span[contains(text(),'Select Diary')])[1]")
	private WebElement labSelectDiary;
	
	@FindBy(xpath = "//div//a//i[@class='fa fa-plus colori']")
	private WebElement labAddDiary;
	
	@FindBy(xpath = "//div//i[@class='fa fa-pencil edit ng-star-inserted']")
	private WebElement labEditDiary;
	
	@FindBy(xpath = "//a//i[contains(@class,'fa fa-check')]")
	private WebElement labDiarySave;
	
	@FindBy(xpath = "(//div//input[contains(@placeholder,'Type')])")
	private WebElement labType;
	
	@FindBy(xpath = "(//div//input[contains(@placeholder,'Site Name')])")
	private WebElement labSiteName;
	
	@FindBy(xpath = "(//div//mat-select[contains(@role,'listbox')])")
	private WebElement labSerActive;
	
	@FindBy(xpath = "//i[@class='fa fa-plus-circle']")
	private WebElement labMasterAdd;
	
	@FindBy(xpath = "//i[@class='fa fa-pencil']")
	private WebElement labEditUser;
	
	@FindBy(xpath = "//i[@class='fa fa-trash ng-star-inserted']")
	private WebElement labDeleteUser;
}
