package stemz.health.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import stemz.health.resource.base.*;

public class PersonalInfo extends ProjectSpecificMethods  {
	private JavascriptExecutor jse = null;
	private WebDriver driver = null;
	
	@FindBy(xpath = "//div//input[@formcontrolname='fname']")
	private WebElement FirstName;
	
	@FindBy(xpath = "//div//input[@formcontrolname='lname']")
	private WebElement LastName;
	
	@FindBy(xpath = "//div//input[@formcontrolname='dob']")
	private WebElement DateOfBirth;
	
	@FindBy(xpath = "//div//input[@formcontrolname='emailID']")
	private WebElement Email;
	
	@FindBy(xpath = "//div//input[@formcontrolname='nationality']")
	private WebElement Nationality;
	
	@FindBy(xpath = "//div//input[@formcontrolname='city']")
	private WebElement City;
	
	@FindBy(xpath = "//div//label[text()='Male']")
	private WebElement GenderMale;
	
	@FindBy(xpath = "//div//label[text()='Female']")
	private WebElement GenderFeMale;
	
	@FindBy(xpath = "//div//input[@formcontrolname='mobile']")
	private WebElement Mobileno;
	
	@FindBy(xpath = "//div//input[@formcontrolname='alternativeMobile']")
	private WebElement AlternativeNo;
	
	@FindBy(xpath = "//div//input[@formcontrolname='country']")
	private WebElement PermanentCountry;
	
	@FindBy(xpath = "//div//button[@id='payFail']")
	private WebElement Back;
	
	
	public PersonalInfo(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
		jse = (JavascriptExecutor) driver;	
	}
	
}
