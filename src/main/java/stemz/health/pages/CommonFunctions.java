package stemz.health.pages;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CommonFunctions {

	private static JavascriptExecutor jse = null;
	private static WebDriver driver = null;
	
	public CommonFunctions(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
		jse = (JavascriptExecutor) driver;	
	}
	
	
	/**
	 * 
	 * @param messageToLog what are the message we passing that is printing in console
	 */
	public static void logMessage(String messageToLog) {

		try {
			System.out.println(messageToLog);
		} catch (Exception e) {
		}
	}
	
	/**
	 * 
	 * @param messageToLog errors that is printing in console
	 */
	public static void logErrorMessage(String messageToLog) {

		try {
			System.err.println(messageToLog);
		} catch (Exception e) {
		}
	}
	
	/**
	 * This method for prints the error messages and stop the execution
	 * 
	 * @param messageToLog this is for print the error message and stop the
	 *                     execution
	 * @throws Exception
	 */
	public static void logErrorMessagestopExecution(String messageToLog) throws Exception {
		System.err.println(messageToLog);
		throw new Exception();
	}
	
	/**
	 * This method to get the date from todays date
	 * 
	 * @param days We have to pass the current date by passing the number of days
	 * @return
	 */
	public static String getDatefromToday(int days) {

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, days);
		return dateFormat.format(cal.getTime());
	}
	
	
	/**
	 * This method for kill the all opened browsers
	 * 
	 * @throws IOException
	 */
	public static void killBrowser() throws IOException {
		try {
			String os = System.getProperty("os.name");
			System.out.println("OS Name - " + os);
			if (os.contains("Windows")) {
				Runtime.getRuntime().exec("taskkill /F /IM firefox.exe");
				Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
				Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");
			}
//			if (os.contains("Linux")) {
//				Runtime.getRuntime().exec("pkill firefox");
//				Runtime.getRuntime().exec("pkill safari");
//			}
			Thread.sleep(2000);
		} catch (Exception e) {
			System.out.println("Exception while closing the browsers");
		}
	}
	
	/** This method for generating the Email randomly **/
	public static String Email_Gen() {

		DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HHmmss");
		Date date = new Date();
		String date1 = dateFormat.format(date);
		String eMail1 = "drupal" + date1 + "@tridentsqa.com";
		System.out.println("New Email Generated  -  " + eMail1);
		return eMail1;
	}
	
	/**
	 * This method for Clicking the Web element
	 * 
	 * @throws Exception
	 **/
	public void clickWebelement(WebElement webele, String Ele_name) throws Exception {
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(webele));
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].click();", webele);
			logMessage("The " + Ele_name + " is clicked");
		} catch (Exception e) {
			logErrorMessagestopExecution("The " + Ele_name + " is not avilable pls refer the report\n");
		}
	}
	
	/** This method for Sending the values to the Web element 
	 * @throws Exception **/
	public void Sendkeys(WebElement webele, String value, String Ele_name) throws Exception {
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(webele));
			webele.click();
			webele.clear();
			webele.sendKeys(value);
			logMessage(value + " is entered into the " + Ele_name + " textbox");
		} catch (Exception e) {
			logErrorMessagestopExecution("\nThe " + Ele_name + " is not avilable pls refer the report\n");
		}

	}
	
	/** This method for Sending the values to the Web element and click Enter
	 * @throws Exception **/ 
	public void SendkeysWithEnter(WebElement webele, String value, String Ele_name) throws Exception {
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(webele));
			webele.click();
			webele.clear();
			webele.sendKeys(value,Keys.ENTER);
			logMessage(value + " is entered into the " + Ele_name + " textbox");
		} catch (Exception e) {
			logErrorMessagestopExecution("\nThe " + Ele_name + " is not avilable pls refer the report\n");
		}

	}
	
	/**
	 * @param ele --> WebElement
	 * @param elementName --> String that to be individually entered
	 * @param strName --> String to be printed as output
	 * 
	 * @throws Exception
	 */
	public void sendKeysIndividual(WebElement ele, String elementName, String strName) throws Exception {
	try {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(ele));
		wait.until(ExpectedConditions.elementToBeClickable(ele));
		ele.click();
		for(int i=0;i<elementName.length();i++) {
			String strChar = new StringBuilder().append(elementName.charAt(i)).toString();
			ele.sendKeys(strChar);
		}
		logMessage(elementName + " is entered into the " + strName + " textbox");
		}catch (Exception e) {
			logErrorMessagestopExecution("\nThe " + strName + " is not avilable pls refer the report\n");
		}
	}
	
	/**
	 * This method for checking the page is displayed or Not
	 * 
	 * @param webele We have to pass the web element to check that is Clickable or
	 *               not
	 * @return
	 */
	public static boolean checkCurrentPage(WebElement webele) {
		try {
			new WebDriverWait(driver, 7).until(ExpectedConditions.elementToBeClickable(webele));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * This Method for checking the page is loaded or not and return the boolean
	 * value true or false
	 * 
	 * @param driver
	 * @throws InterruptedException
	 */
	public static void waitForPageLoad(WebDriver driver) throws InterruptedException {
		Thread.sleep(4000);
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(pageLoadCondition);
	}
	
	/**
	 * This method for switching the one window to next window
	 * 
	 * @param driver
	 * @throws InterruptedException
	 */
	public static void switchNextWindow(WebDriver driver) throws InterruptedException {
		Thread.sleep(4000);
		int i = 0;
		for (String Child_Window : driver.getWindowHandles()) {
			if (i > 0) {
				driver.switchTo().window(Child_Window);
			}
			i++;
		}
	}
	
	/**
	 * This Method for checking the web element is visible or not
	 * 
	 * @param webele pass the web element.
	 * @param name   we have to pass the name of the element if it is not displayed
	 *               it prints the error message
	 * @throws Exception
	 */
	public static boolean elementIsVisible(WebElement webele, String name) throws Exception {
		try {
			new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(webele));
			return true;
		} catch (Exception e) {
			return false;
		}

	}
	
	/**
	 * This Method for checking the web element is clickable or not
	 * 
	 * @param ele     pass the web element.
	 * @param objname we have to pass the name of the element if it is not displayed
	 *                it prints the error message
	 * @throws Exception
	 */
	public static void elementToBeClickable(WebElement ele, String objname) throws Exception {

		try {
			new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(ele));
		} catch (Exception e) {
			logErrorMessagestopExecution("The object " + objname + " is not clickable");
		}
	}
	
	/**
	 * @param webele
	 * @param objName
	 */
	public static String getTextOfElement(WebElement webele, String objName) {
		String elementText = null;
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(webele));
			elementText = webele.getText();
		} catch (Exception e) {
			logErrorMessage("The Object " + objName + " is not available.");
		}
		return elementText;
	}
	
	/**
	 * @param webele
	 * @param objName
	 * @param attributeName    This method will Get the Attribute of the Element
	 * @return
	 */
	public static String getAttributeOfElement(WebElement webele, String objName, String attributeName) {
		String element = null;
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(webele));
			element = webele.getAttribute(attributeName);
		} catch (Exception e) {
			logErrorMessage("The Object " + objName + " is not available.");
		}
		return element;
	}
	
	/**
	 * This method for Switching from one frame another frame using frame name
	 * 
	 *
	 * @param framename We have to pass the frameId to check if it is available or
	 *                  not
	 * @return
	 * @throws Exception
	 */
	public static void switchFrame(String framename) {

		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame(framename);
		} catch (Throwable ex) {
			System.out.println("Error occured while switching the frame");
			System.out.println(ex.getMessage());
		}
	}
	
	public static String getRamdonString(int chars) {
		String CharSet = "abcdefghijkmnopqrstuvwxyz";
		String name = "";
		for (int a = 1; a <= chars; a++) {
			name += CharSet.charAt(new Random().nextInt(CharSet.length()));
		}
		return name;
	}
	
	/**
	 * This method is used for generating random number for based on length
	 * 
	 * @param length
	 * @return
	 */
	public static String generateRandomNumber(int length) {
		return RandomStringUtils.randomNumeric(length);
	}
	
	/**
	 * This method is used for generating random Alpha Numeric
	 * string for based on length
	 * 
	 * @param length
	 * @return
	 */
	public static String generateRandomAlphaNumeric(int length) {
		return RandomStringUtils.randomAlphanumeric(length);
	}
	
	/**
	 * This method is used for click the element using Javascript
	 * 
	 * @throws Exception
	 */
	public static void clickJSE(WebElement ele, String elementName) throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(ele));
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			jse.executeScript("arguments[0].click();", ele);
			logMessage("iClick on the element: " + elementName);
		} catch (WebDriverException e) {
			logErrorMessagestopExecution("i failed to Click on the element: " + elementName);
		}
	}


}
