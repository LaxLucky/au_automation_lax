package stemz.health.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import stemz.health.resource.base.*;

public class LoginPage extends ProjectSpecificMethods {
	private JavascriptExecutor jse = null;
	private WebDriver driver = null;
	
	@FindBy(xpath = "//span[text()='Login as Guest']")
	private WebElement GuestLogin;
	
	@FindBy(xpath = "//div//input[@placeholder='User Name']")
	private WebElement Username;
	
	@FindBy(xpath = "//div//input[@placeholder='Password']")
	private WebElement UserPassword;
	
	@FindBy(xpath = "//div//button//span[contains(text(),'Login')]")
	private WebElement UserLogin;
	
	@FindBy(xpath = "//button//div[contains(text(),' OTP ')]")
	private WebElement UserOtp;
	
	@FindBy(xpath = "//div//a[contains(text(),'Forgot Password')]")
	private WebElement UserForgotPassword;
	
	public LoginPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
		 jse = (JavascriptExecutor) driver;
	}
	
	public void setusername(String data) throws InterruptedException
	{
		Thread.sleep(5000);
		Username.sendKeys(data);
	}
	
	public void setpassword(String data) throws InterruptedException
	{
		Thread.sleep(5000);
		UserPassword.sendKeys(data);
	}
	
	public void clickLogin()
	{
		UserLogin.click();
	}
	public void guestLogin()
	{
		GuestLogin.click();
	}
}
