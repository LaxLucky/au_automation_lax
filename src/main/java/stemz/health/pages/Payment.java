package stemz.health.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import stemz.health.resource.base.*;

public class Payment extends ProjectSpecificMethods  {
	private JavascriptExecutor jse = null;
	private WebDriver driver = null;
	
	@FindBy(xpath = "//button[@id='rzp-button1']")
	private WebElement Makepayment;
	
	@FindBy(xpath = "(//span[text()='Back'])[4]")
	private WebElement Back;
	
	public Payment(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
		jse = (JavascriptExecutor) driver;	
	}
	
	
}
