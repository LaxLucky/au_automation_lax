package stemz.health.pages;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import stemz.health.resource.base.*;

public class TravelInfo extends ProjectSpecificMethods {		
		private JavascriptExecutor jse = null;
		private WebDriver driver = null;
		
		@FindBy(xpath = "//button//div[contains(text(),' Domestic Travel ')]")
		private WebElement domesticTravel;
		
		@FindBy(xpath = "//button//div[contains(text(),'International Travel')]")
		private WebElement internationlTravel;
		
		@FindBy(xpath = "//div//input[contains(@formcontrolname,'pnrno')]")
		private WebElement pnrno;
		
		@FindBy(xpath = "//div//input[contains(@formcontrolname,'airlines')]")   
		private WebElement Airline;
		
		@FindBy(xpath = "//mat-option")
		private List<WebElement> OptionSelect;
		
		@FindBy(xpath = "//div//input[contains(@formcontrolname,'flightNumber')]")
		private WebElement FlightNumber;
		
		@FindBy(xpath = "//div//mat-select[contains(@formcontrolname,'identityType')]")
		private WebElement IdentityType;
		
		@FindBy(xpath = "//input[contains(@formcontrolname,'identityNumber')]")
		private WebElement IdentityNumber;
		
		@FindBy(xpath = "//div//input[contains(@formcontrolname,'passportNo')]")
		private WebElement PassportNumber;
		
		@FindBy(xpath = "//div//input[contains(@formcontrolname,'departureOrigin')]")
		private WebElement Departure;
		
		@FindBy(xpath = "//div//input[contains(@formcontrolname,'arrivalOrigin')]")
		private WebElement Arrival;
		
		@FindBy(xpath = "//div//input[contains(@formcontrolname,'departureDate')]")
		private WebElement DepartureDate;
		
		@FindBy(xpath = "//div//input[contains(@formcontrolname,'arrivalDate')]")
		private WebElement ArrivalDate;
		
		@FindBy(xpath = "//td[not(@aria-disabled='true') and (@role)]")
		private List<WebElement> availDate;
		
		@FindBy(xpath = "(//button//span[contains(text(),'Next')])[1]")
		private WebElement Next;
		
		public TravelInfo(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
			jse = (JavascriptExecutor) driver;	
		}
		public void travelType() throws Exception {
			CommonFunctions.get().clickWebelement(domesticTravel, "domestic travel");
				
		}
		public void pnrNumber(String pnr) throws Exception {
			CommonFunctions.get().Sendkeys(pnrno, pnr, "PNR");
		}
		
		public void airlineName(String airline) throws Exception {
			CommonFunctions.get().Sendkeys(Airline, airline, "Airline");
			OptionSelect.get(0).click();
		}
		
		@SuppressWarnings("static-access")
		public void identityType(String idType) throws Exception {
			CommonFunctions.get().elementToBeClickable(IdentityType, "Identity Type");
			IdentityType.click();
			switch(idType.toLowerCase()) {
			case "aadhar":
				CommonFunctions.get().clickWebelement(OptionSelect.get(1), "Aadhar card");
				break;
				
			case "passport":
				CommonFunctions.get().clickWebelement(OptionSelect.get(0), "Passport");
				break;
				
			case "voter":
				CommonFunctions.get().clickWebelement(OptionSelect.get(2), "Voter Id");
				break;
				
			default:
				CommonFunctions.get().logErrorMessagestopExecution("Entered incorrect identity type");
				break;
			}
		}
		
		public void identityNumber(String idNumber) throws Exception {
			CommonFunctions.get().sendKeysIndividual(IdentityNumber, idNumber, "identity number");
		}
		
		public void departureCity(String departCity) throws Exception {
			CommonFunctions.get().Sendkeys(Departure, departCity, "departure city");
			OptionSelect.get(0).click();
		}
		
		public void arrivalCity(String arriveCity) throws Exception {
			CommonFunctions.get().Sendkeys(Arrival, arriveCity, "arrival city");
			OptionSelect.get(0).click();
		}
		
		@SuppressWarnings("static-access")
		public void departureDate(String date) throws Exception {
			CommonFunctions.get().elementToBeClickable(DepartureDate, "Departure Date");
			DepartureDate.click();
			CommonFunctions.get().clickWebelement(availDate.get(Integer.parseInt(date)), "Departure Date");
		}
		
		@SuppressWarnings("static-access")
		public void arrivalDate(String date) throws Exception {
			CommonFunctions.get().elementToBeClickable(ArrivalDate, "Arrival Date");
			ArrivalDate.click();
			CommonFunctions.get().clickWebelement(availDate.get(Integer.parseInt(date)), "Arrival Date");
		}
		
		public void nextButton() throws Exception {
			CommonFunctions.get().clickWebelement(Next, "Next Button");
		}
		
		public void fillTravelInfoPage(String pnr, String airName, String idType, String idNum, String departCity, String arriveCity, String departDate, String arriveDate) throws Exception {
			travelType();
			pnrNumber(pnr);
			airlineName(airName);
			identityType(idType);
			identityNumber(idNum);
			departureCity(departCity);
			Thread.sleep(2000);
			arrivalCity(arriveCity);
			departureDate(departDate);
			arrivalDate(arriveDate);
			nextButton();
		}
}


