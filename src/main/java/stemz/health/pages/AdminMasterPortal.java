package stemz.health.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import stemz.health.resource.base.*;

public class AdminMasterPortal extends ProjectSpecificMethods {
	private JavascriptExecutor jse = null;
	private WebDriver driver = null;
	
	@FindBy(xpath = "//input[@id='mat-input-24']")
	private WebElement adminUsername;
	
	@FindBy(xpath = "(//div[@class='mat-form-field-infix'])[2]")
	private WebElement adminSite;
	
	@FindBy(xpath = "//div[@class='mat-select-value']")
	private WebElement adminUserType;
	
	@FindBy(xpath = "//i[@title='edituser']")
	private WebElement adminEditUser;
	
	@FindBy(xpath = "//i[@title='resetpass']")
	private WebElement adminResetPass;
	
	@FindBy(xpath = "//i[@title='deleteuser']")
	private WebElement adminDeleteUser;
	
	@FindBy(xpath = "//i[@class='fa fa-search']")
	private WebElement adminSearch;
	
	@FindBy(xpath = "//i[@class='fa fa-user-plus']")
	private WebElement adminUserPlus;
	
	@FindBy(xpath = "//i[@class='fa fa-user-circle-o']")
	private WebElement adminUser;
	
	@FindBy(xpath = "//i[@class='fa fa fa-map-marker']")
	private WebElement adminMapMarker;
	
	@FindBy(xpath = "//i[@class='fa fa-flask']")
	private WebElement adminLabMaster;
	
	@FindBy(xpath = "//i[@class='fa fa-briefcase']")
	private WebElement adminServiceMaster;
	
	@FindBy(xpath = "//i[@class='fa fa-plane']")
	private WebElement adminAirLine;
	
	@FindBy(xpath = "//i[@class='fa fa-book']")
	private WebElement adminDiary;
	
	@FindBy(xpath = "//i[@class='fa fa-paper-plane']")
	private WebElement adminDepartureAirport;
	
	@FindBy(xpath = "//i[@class='fa fa-map-signs']")
	private WebElement adminArrivalAirport;
	
	@FindBy(xpath = "//i[@class='fa fa-calendar-o']")
	private WebElement adminEvent;
	
	@FindBy(xpath = "//i[@class='fa fa-check-square-o']")
	private WebElement adminConsultaionhrs;
	
	@FindBy(xpath = "//i[@class='fa fa-th-list']")
	private WebElement adminStemzPartners;
	
	@FindBy(xpath = "//i[@class='fa fa-commenting']")
	private WebElement adminSmsTemplate;
	
	@FindBy(xpath = "//i[@class='fa fa-fighter-jet']")
	private WebElement adminAirlinePartner;
	
	@FindBy(xpath = "//i[@class='fa fa-envelope']")
	private WebElement adminEmailTemplate;
	
	@FindBy(xpath = "//i[@class='fa fa-product-hunt']")
	private WebElement adminPhysician;
	
	@FindBy(xpath = "//a[@href='#/changepassword']")
	private WebElement adminChangePassword;
	
	@FindBy(xpath = "//a[@href='#/login']")
	private WebElement adminLogin;
	
	@FindBy(xpath = "(//div//input[@name='search'])[1]")
	private WebElement adminUserNameSearch;
	
	@FindBy(xpath = "(//div//input[@name='search'])[2]")
	private WebElement adminUserCitySearch;
	
	@FindBy(xpath = "(//div//input[@name='search'])[3]")
	private WebElement adminUserTypeSearch;
	
	@FindBy(xpath = "//div[@class='mat-select-value']")
	private WebElement adminUserActiveStatus;
	
	@FindBy(xpath = "//div//a//i[@class='fa fa-plus-circle']")
	private WebElement adminUserAddSiteMaster;
	
	@FindBy(xpath = "(//div//span[contains(text(),'Select Diary')])[1]")
	private WebElement adminSelectDiary;
	
	@FindBy(xpath = "//div//a//i[@class='fa fa-plus colori']")
	private WebElement adminAddDiary;
	
	@FindBy(xpath = "//div//i[@class='fa fa-pencil edit ng-star-inserted']")
	private WebElement adminEditDiary;
	
	@FindBy(xpath = "(//div//input[contains(@class,'mat-input-element')])[1]")
	private WebElement adminDairyName;
	
	@FindBy(xpath = "//span[@class='ng-tns-c14-41 ng-star-inserted']")
	private WebElement adminDiarySelectSiteName;
	
	@FindBy(xpath = "//div//input[@placeholder='Old Password']")
	private WebElement adminOldPassword;
	
	@FindBy(xpath = "//div//input[@placeholder='New Password']")
	private WebElement adminNewPassword;
	
	@FindBy(xpath = "//div//input[@placeholder='Confirm Password']")
	private WebElement adminConfirmPassword;
	
	@FindBy(xpath = "//div//button[contains(text(),' Save ')]")
	private WebElement adminSave;
	
	public AdminMasterPortal(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
		jse = (JavascriptExecutor) driver;
	}

}
