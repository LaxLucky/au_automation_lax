package stemz.health.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import stemz.health.resource.base.*;

public class AirLineUser extends ProjectSpecificMethods  {
	
	private JavascriptExecutor jse = null;
	private WebDriver driver = null;
		
	@FindBy(xpath = "//a[@ng-reflect-router-link='/genericReport']")
	private WebElement AirlineTravelerList;
	
	@FindBy(xpath = "//a[@ng-reflect-router-link='/pnr']")
	private WebElement AirlinePNR;
	
	@FindBy(xpath = "//a[@ng-reflect-router-link='/usermanagement']")
	private WebElement AirlineUser;
	
	@FindBy(xpath = "//a[@ng-reflect-router-link='/airlinepartner']")
	private WebElement AirlinePartner;
	
	@FindBy(xpath = "//div//span//mat-label[text()='Name']")
	private WebElement AirlineName;
	
	@FindBy(xpath = "//div//span//mat-label[text()='PNR No']")
	private WebElement AirlinePNRs;
	
	@FindBy(xpath = "//div//span//mat-label[text()='Health Passport No']")
	private WebElement AirlinePassportNo;
	
	@FindBy(xpath = "//div//span//mat-label[text()='Mobile No']")
	private WebElement AirlineMobileNo;
	
	@FindBy(xpath = "//div//span//mat-label[text()='Flight No']")
	private WebElement AirlineFlightNo;
	
	@FindBy(xpath = "//div//p[contains(text(),'Domestic')]")
	private WebElement AirlineDomestic;
	
	@FindBy(xpath = "//div//p[contains(text(),'International')]")
	private WebElement AirlineInternational;
	
	@FindBy(xpath = "//div//span[@class='mat-button-wrapper' and contains(text(),'Search')]")
	private WebElement AirlineSearch;
	
	@FindBy(xpath = "//div//span[@class='mat-button-wrapper' and contains(text(),'Clear')]")
	private WebElement AirlineClear;
	
	@FindBy(xpath = "//div//input[@placeholder='Health Passport no']")
	private WebElement AirlineHealthPassportSearch;
	
	@FindBy(xpath = "//div//input[@placeholder='PNR Number']")
	private WebElement AirlinePNRSearch;
	
	@FindBy(xpath = "//div//input[@placeholder='Passport number']")
	private WebElement AirlinePasswordSearch;
	
	@FindBy(xpath = "//div//button[text()='Search']")
	private WebElement AirlinePnrSearch;
	
	@FindBy(xpath = "(//div[contains(@class,'mat-form-field-infix')]//span[text()='Site'])")
	private WebElement AirlineUserSite;
	
	@FindBy(xpath = "(//div[contains(@class,'mat-select-value')])")
	private WebElement AirlineUserType;
	
	@FindBy(xpath = "(//div[contains(@class,'mat-form-field-infix')])[1]")
	private WebElement AirlineUserName;
	
	@FindBy(xpath = "//i[@class='fa fa-plus-circle']")
	private WebElement AirlineAddUser;
	
	@FindBy(xpath = "(//i[@class='fa fa-search'])[2]")
	private WebElement AirlineUserSearch;
	
	@FindBy(xpath = "(//div//input[@name='search'])[1]")
	private WebElement  AirlineUserNameSearch;
	
	@FindBy(xpath = "(//div//input[@name='search'])[2]")
	private WebElement  AirlineUserCitySearch;
	
	@FindBy(xpath = "//i[@class='fa fa-pencil']")
	private WebElement AirlineEditUser;
	
	@FindBy(xpath = "//i[@class='fa fa-trash ng-star-inserted']")
	private WebElement AirlineDeleteUser;
	
	@FindBy(xpath = "(//div//mat-select[contains(@role,'listbox')])")
	private WebElement AirlineActive;
	
	public AirLineUser(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
		jse = (JavascriptExecutor) driver;	
	}
}


