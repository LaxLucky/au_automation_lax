package stemz.health.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import stemz.health.resource.base.*;

public class Appointment extends ProjectSpecificMethods  {
	private JavascriptExecutor jse = null;
	private WebDriver driver = null;
	
	@FindBy(xpath = "//div[contains(text(),' Home Visit ')]")
	private WebElement HomeVisit;
	
	@FindBy(xpath = "//div[contains(text(),' Lab Visit ')]")
	private WebElement LabVisit;
	
	@FindBy(xpath = "//div//input[@formcontrolname='cityLab']")
	private WebElement LabCity;
	
	@FindBy(xpath = "//div//input[@formcontrolname='streetone']")
	private WebElement FltNo;
	
	@FindBy(xpath = "//div//input[@formcontrolname='streettwo']")
	private WebElement StreetName;
	
	@FindBy(xpath = "//div//input[@formcontrolname='area']")
	private WebElement LandMark;
	
	@FindBy(xpath = "//div//input[@formcontrolname='pincode']")
	private WebElement Pincode;
	
	@FindBy(xpath = "//div//input[@formcontrolname='checkTerm']")
	private WebElement Termsconditions;
	
	@FindBy(xpath = "//div//input[@formcontrolname='checkRegulation']")
	private WebElement PrivacyPolicy;
	
	@FindBy(xpath = "//div//button[@id='tabapp']")
	private WebElement Next;
	
	@FindBy(xpath = "(//button[@type='button']//span[contains(text(),'Back')])[1]")
	private WebElement Back;
	
	@FindBy(xpath = "//div//mat-select[@formcontrolname='laboratory']")
	private WebElement Laboratory;
	
	public Appointment(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
		jse = (JavascriptExecutor) driver;	
	}
}
