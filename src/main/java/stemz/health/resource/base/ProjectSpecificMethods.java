package stemz.health.resource.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import stemz.health.pages.*;

import stemz.health.utilis.ExtentManager;
import stemz.health.utilis.InvokeBrowser;
import stemz.health.utilis.TestUtil;
import stemz.health.utilis.Xls_Reader;
import org.testng.SkipException;

public class ProjectSpecificMethods {
	
	public static boolean isInitialized = false;
	public static Xls_Reader AAxls = null;
	public static String Excelpath = "";
	public static String sheetName = "";
	public String excelFileName;
	
	public static ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();
	public static ThreadLocal<AdminMasterPortal> AdminMasterPortal = new ThreadLocal<AdminMasterPortal>();
	public static ThreadLocal<AirLineUser> AirLineUser = new ThreadLocal<AirLineUser>();
	public static ThreadLocal<Appointment> Appointment = new ThreadLocal<Appointment>();
	public static ThreadLocal<CallCentrePortal> CallCentrePortal = new ThreadLocal<CallCentrePortal>();
	public static ThreadLocal<DoctorConsult> DoctorConsult = new ThreadLocal<DoctorConsult>();
	public static ThreadLocal<LabMasterPortal> LabMasterPortal = new ThreadLocal<LabMasterPortal>();
	public static ThreadLocal<LoginPage> LoginPage = new ThreadLocal<LoginPage>();
	public static ThreadLocal<Payment> Payment = new ThreadLocal<Payment>();
	public static ThreadLocal<PersonalInfo> PersonalInfo = new ThreadLocal<PersonalInfo>();
	public static ThreadLocal<TravelInfo> TravelInfo = new ThreadLocal<TravelInfo>();
	public static ThreadLocal<InvokeBrowser> InvokeBrowser = new ThreadLocal<InvokeBrowser>(); 
	public static ThreadLocal<CommonFunctions> CommonFunctions = new ThreadLocal<CommonFunctions>();
	protected static ThreadLocal<ExtentTest> test = new ThreadLocal<>();
	
	private String browserNeeded = null;
	private boolean skip = false;
	protected static ExtentReports extent;
	public static String browser = System.getenv("Browser");
	
	
	@DataProvider(name = "StemzExecution", parallel = false)
	public Object[][] getTestData() {
	return TestUtil.getData(AAxls, "StemzBook");
	}
	
	@BeforeTest
	public void assignBrowser() throws Exception {
		initialize();
		sheetName = "StemzBook";
		if (!TestUtil.isTestCaseRunnable(AAxls, sheetName)) {
			throw new SkipException("Skipping Test Case smoke as runmode set to NO");// reports
		}
		extent = ExtentManager.createInstance();

		selectBrowser();
		
	}
	
	public void selectBrowser() {
		
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\src\\main\\java\\stemz\\health\\resource\\chromedriver.exe");
					driver.set(new ChromeDriver());
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver",
					System.getProperty("user.dir") + "\\src\\main\\java\\stemz\\health\\resource\\geckodriver.exe");
					driver.set(new FirefoxDriver());
		}
		loadingDriver();
	}
	
	public void loadingDriver() {

		AdminMasterPortal.set(new AdminMasterPortal(driver.get()));
		AirLineUser.set(new AirLineUser(driver.get()));
		Appointment.set(new Appointment(driver.get()));
		CallCentrePortal.set(new CallCentrePortal(driver.get()));
		DoctorConsult.set(new DoctorConsult(driver.get()));
		LabMasterPortal.set(new LabMasterPortal(driver.get()));
		LoginPage.set(new LoginPage(driver.get()));
		Payment.set(new Payment(driver.get()));
		PersonalInfo.set(new PersonalInfo(driver.get()));
		TravelInfo.set(new TravelInfo(driver.get()));
		InvokeBrowser.set(new InvokeBrowser(driver.get()));
		CommonFunctions.set(new CommonFunctions(driver.get()));
	}
	
	public void initialize() throws Exception {

		if (!isInitialized) {

			String testXLS = System.getenv("TEST_XLS");
			System.out.println("Running Excel sheet " + testXLS);

			if (testXLS != null | testXLS.length() != 0) {
				AAxls = new Xls_Reader(
						System.getProperty("user.dir") + "\\src\\main\\java\\stemz\\health\\resource\\xlsheets\\"+testXLS);
				Excelpath = System.getProperty("user.dir") + "\\src\\main\\java\\stemz\\health\\resource\\xlsheets\\"+testXLS;	
			} else {
				System.out.println("TEST_XLS Environment Variable is not Configured");
			}
			isInitialized = true;
			
		}
	}

/**
* This method is responsible for closing of all the browsers and printing
* report.
*
* @param result
* @throws UnsupportedEncodingException
*/
		@AfterTest
		public void flush() {
		extent.flush();
		driver.get().quit();
		}
	
}
