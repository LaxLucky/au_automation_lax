package stemz.health.utilis;

	import org.testng.ITestContext;
	import org.testng.ITestListener;
	import org.testng.ITestResult;

	import stemz.health.resource.base.ProjectSpecificMethods;

	public class TestListener extends ProjectSpecificMethods implements ITestListener {
		
	//Extent Report Declarations

	@Override
	public synchronized void onStart(ITestContext context) {

	}

	@Override
	public synchronized void onFinish(ITestContext context) {

	if (context.getFailedTests().size() >= 1) {
	//Mail.sendReport();
	}else if(context.getPassedTests().size() == 6 ) {
	//Mail.sendReport();
	}
	}

	@Override
	public synchronized void onTestStart(ITestResult result) {

	}

	@Override
	public synchronized void onTestSuccess(ITestResult result) {

	test.get().pass("Test passed");
	}

	@Override
	public synchronized void onTestFailure(ITestResult result) {
	test.get().fail(result.getThrowable());
	// test.get().fail("Runmode is set No for the testcase");
	}

	@Override
	public synchronized void onTestSkipped(ITestResult result) {

	test.get().skip(result.getThrowable());
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

	}

}
