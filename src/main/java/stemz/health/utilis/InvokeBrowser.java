package stemz.health.utilis;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import stemz.health.resource.base.*;

public class InvokeBrowser extends ProjectSpecificMethods {
	
	private WebDriver driver = null;
	
	public InvokeBrowser(WebDriver driver) {
		this.driver = driver;
		}
	
	public void invokeApplication(String platform) {
		System.out.println("Inside------------");
		String baseurl="";
		
		switch(platform) {
		 case "PassengerCheckPortal":
			 baseurl="http://stemzglobalcovidqa.azurewebsites.net/portal/index.html#/login";
			 break;
		
		 case "BookPortal":
			 baseurl="http://stemzglobalcovidqa.azurewebsites.net/portal/index.html#/booking";
			 break;
			 
		 case "SuperAdminPortal":
			 baseurl="http://stemzglobalcovidqa.azurewebsites.net/hisbase/index.html";
			 break;
			 
		 default:
				System.out.println("Wrong platform selected");
				break;
		}
		
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		System.out.println(baseurl);
		driver.get(baseurl);
	}

}
